﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateColor : MonoBehaviour
{
    public Material[] materials;
    public GameObject[] Cube;

    
    public Color Color;
    // Start is called before the first frame update
    void Start()
    {
        Cube = GameObject.FindGameObjectsWithTag("Cube");

    }

    // Update is called once per frame
    public void ColorsCreator(GameObject G)
    {
        int c = 1;
        foreach(Material m in materials)
        {
            Debug.Log("Je cherche la couleur de " +G.name);
            if(G.GetComponent<MeshRenderer>().sharedMaterial == m)
            {
                Debug.Log("J'ai trouvé la couleur");
                //G.GetComponent<MeshRenderer>().material.GetColor("_Color");
                G.GetComponent<MeshRenderer>().material.color = G.GetComponent<MeshRenderer>().material.color - new Color(c,0,0,0);
                c = -c;
                G.GetComponent<MeshRenderer>().material.color = G.GetComponent<MeshRenderer>().material.color - new Color(0,c,0,0);
            }
            else{
                Debug.Log("Ce n'est pas la bonne couleur , je continue");
                Debug.Log(G.GetComponent<MeshRenderer>().sharedMaterial);
            }
        }
        
    }
}
