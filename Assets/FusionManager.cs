﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FusionManager : MonoBehaviour
{

    public GameObject CubeComplete;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "Cube")
        {
            if(col.gameObject.transform.localScale.x == 0.5f)
            {
                var Obj = Instantiate(CubeComplete, this.transform.position, Quaternion.identity);
                Obj.GetComponent<MeshRenderer>().material.color = this.GetComponent<MeshRenderer>().sharedMaterial.color + col.gameObject.GetComponent<MeshRenderer>().sharedMaterial.color;
                Destroy(gameObject);
                Destroy(col.gameObject);
            }
        }
    }
}
