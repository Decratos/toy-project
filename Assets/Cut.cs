﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EzySlice;

public class Cut : MonoBehaviour
{
    public List<GameObject> objectToSlice; // non-null
    public Camera camera;
    public Vector3[] PlanePosition = new Vector3[2];
    public GameObject Plane;
    MeshGenerator mesh;
    public Vector3 CutPosition;
    public Vector3 CutDirection;
    // Start is called before the first frame update
    void Start()
    {
        mesh = GameObject.Find("Mesh").GetComponentInChildren<MeshGenerator>();
    }

    // Update is called once per frame
    void Update()
    {
        
        PlanePosition[2] =  new Vector3(camera.transform.position.x - 3.45f, camera.transform.position.y, camera.transform.position.z); 
        PlanePosition[3] =  new Vector3(camera.transform.position.x+ 3.45f,camera.transform.position.y, camera.transform.position.z); 
        if(Input.GetMouseButtonDown(0))
        {
            
            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
           
            // Does the ray intersect any objects excluding the player layer
            if (Physics.Raycast(ray, out hit))
            {
                Debug.DrawLine(ray.origin, hit.point, Color.yellow);
                
                PlanePosition[0] = hit.point;
            }
            else
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
                Debug.Log("Did not Hit");
            }
            //PlanePosition[0] = 
        }
        if(Input.GetMouseButtonUp(0))
        {
            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            // Does the ray intersect any objects excluding the player layer
            if (Physics.Raycast(ray, out hit))
            {
                
                Debug.DrawLine(ray.origin, hit.point, Color.yellow);
                PlanePosition[1] = hit.point; 
                foreach(GameObject G in objectToSlice)
                {
                    if(G == null)
                    {
                        objectToSlice.Remove(G);
                    }
                }
                mesh.CreateShape();
                  
            }
            else
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
                Debug.Log("Did not Hit");
            }
            
        }
    
    }

    

    

    /**
    * Example on how to slice a GameObject in world coordinates.
    */
    public SlicedHull SliceObject(Vector3 planeWorldPosition,GameObject ile, Material mat = null) {
	    return ile.Slice(CutPosition, CutDirection, mat);
        
}


public void Decoupe(GameObject mesh, Vector3 vertexNormal)
{
    CutPosition = vertexNormal;
    CutDirection = vertexNormal;
   if(objectToSlice.Count > 0)
   {
       foreach(GameObject ILE in objectToSlice)
    {
        Debug.Log("Je commence à couper");
        SlicedHull hull = SliceObject(CutPosition, ILE, null);
        if(hull != null)
        {
            Debug.Log("Coupe Coupe");
            GameObject Up = hull.CreateUpperHull(ILE);
            Up.AddComponent<Rigidbody>();
            Up.AddComponent<BoxCollider>();
            Up.GetComponent<Rigidbody>().AddForce(Up.transform.up * Time.deltaTime, ForceMode.Acceleration);
            GameObject Bottom = hull.CreateLowerHull(ILE);
            Bottom.AddComponent<Rigidbody>();
            Bottom.AddComponent<BoxCollider>();
            Bottom.GetComponent<Rigidbody>().AddForce(Bottom.transform.up * Time.deltaTime, ForceMode.Acceleration);
            Destroy(ILE);
                    
                
            }
            else{
                Debug.Log("c'est null");
            }
        }
        
            
    }
     
   } 
  
    
}


