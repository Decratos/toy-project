﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIBehavior : MonoBehaviour
{
    NavMeshAgent agent;
    public bool isInfected = false;
    // Start is called before the first frame update
    void Start()
    {
            gameObject.GetComponent<Rigidbody>().isKinematic = true;
            agent = GetComponent<NavMeshAgent>();
            float randX = Random.Range(-15f, 10);
            float randY = Random.Range(18.5f, -8f);
            agent.SetDestination(new  Vector3(randX, 0, randY));
    }

    // Update is called once per frame
    void Update()
    {
        if(agent.enabled == true && agent.remainingDistance < 1f)
        {
            Debug.Log("J'y vais");
            float randX = Random.Range(-15f, 10);
            float randY = Random.Range(18.5f, -8f);
            agent.SetDestination(new  Vector3(randX, 0, randY));
        }
    }

    void OnCollisionEnter(Collision Col)
    {
        if(Col.transform.tag == "Proj" && Col.rigidbody.velocity.magnitude > 3)
        {
            Debug.Log(Col.rigidbody.velocity.magnitude);
            Rigidbody[] Rig = GetComponentsInChildren<Rigidbody>();
            agent.enabled = false;
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
            foreach(Rigidbody R in Rig)
            {
                R.isKinematic = false;
            }
            StartCoroutine(Wait());
        }
        if (Col.transform.tag == "Fragment" && isInfected)
        {
            Debug.Log(Col.rigidbody.velocity.magnitude);
            Rigidbody[] Rig = GetComponentsInChildren<Rigidbody>();
            agent.enabled = false;
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
            foreach (Rigidbody R in Rig)
            {
                R.isKinematic = false;
            }
            StartCoroutine(Wait());
        }

    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }
}
