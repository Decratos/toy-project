﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class MeshGenerator : MonoBehaviour
{
    int [] triangles;
    Vector3[] vertices;
    Cut c;
    Mesh mesh;
    GameObject Plane;
    // Start is called before the first frame update
    void Start()
    {
        c = Camera.main.GetComponent<Cut>();
       Plane = GameObject.Find("Plane");
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void CreateShape()
    {
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;
        vertices = c.PlanePosition;
        triangles = new int[]
        {
            0,1,2,
            1,3,2
        };
        UpdateMesh();
    }

    public void UpdateMesh()
    {
        //mesh.Clear();
        for (int i = 0; i<c.PlanePosition.Length; i++)
        {
            vertices[i] = c.PlanePosition[i] - this.transform.position;
        }
        mesh.vertices = vertices; //Crossproduct de deux vector entre Ab et AC = normal d'un vertex , = vector up et position
        mesh.triangles = triangles;
        
        
        if(this.gameObject.GetComponent<MeshCollider>() == null)
        {
            this.gameObject.AddComponent<MeshCollider>();
            this.GetComponent<MeshCollider>().convex = true;
            this.GetComponent<MeshCollider>().isTrigger = true;
        }
        else{
            GetComponent<MeshCollider>().sharedMesh = null;
            GetComponent<MeshCollider>().sharedMesh = GetComponent<MeshFilter>().mesh;  
        }
        
       
    }

    void OnTriggerEnter(Collider col)
{
    if(col.gameObject.tag == "Cube")
    {
        c.objectToSlice.Add(col.gameObject);
        Vector3 dirPos = GetVector(vertices[2], vertices[0], vertices[3]);
        c.Decoupe(this.gameObject, dirPos);
    }
        
}

    void OnTriggerExit(Collider col)
{
    if(col.gameObject.tag == "Cube")
    c.objectToSlice.Remove(col.gameObject);
}
    Vector3 GetVector(Vector3 a, Vector3 b, Vector3 c)
    {
        Vector3 side1 = (Vector3.Normalize(b) + new Vector3(0, 25, 0)) - (Vector3.Normalize(a)+ new Vector3(0, 25, 0));
        Vector3 side2 = (Vector3.Normalize(c) + new Vector3(0, 25, 0)) - (Vector3.Normalize(a)+ new Vector3(0, 25, 0));
       // Debug.DrawLine(c, a, Color.blue, 10f);
        //Debug.DrawLine(b, a, Color.blue, 10f);
        Debug.DrawLine(side1, side2, Color.blue, 100f);
        Debug.DrawLine(Vector3.Cross(side1, side2), a, Color.blue, 10f);
        return Vector3.Cross(side1, side2);

    }

    
}
