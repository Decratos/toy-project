﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlane : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
     if(Input.GetKey(KeyCode.RightArrow))
     {
         transform.eulerAngles += new Vector3(0,0,(-10) * Time.deltaTime);
     }   
     else if(Input.GetKey(KeyCode.LeftArrow))
     {
         transform.eulerAngles += new Vector3(0,0,(10) * Time.deltaTime);
     }
    }
}
