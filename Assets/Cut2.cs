﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EzySlice;

public class Cut2 : MonoBehaviour
{
    public List<GameObject> objectToSlice; // non-null
    public GameObject Plane;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            foreach(GameObject ILE in objectToSlice)
            {
                SlicedHull hull = SliceObject(this.transform.position, ILE, null);
                if(hull != null)
                {
                
                    GameObject Up = hull.CreateUpperHull(ILE);
                    Up.AddComponent<Rigidbody>();
                    Up.AddComponent<BoxCollider>();
                    Up.GetComponent<Rigidbody>().AddForce(Up.transform.up * Time.deltaTime, ForceMode.Acceleration);
                    GameObject Bottom = hull.CreateLowerHull(ILE);
                    Bottom.AddComponent<Rigidbody>();
                    Bottom.AddComponent<BoxCollider>();
                    Bottom.GetComponent<Rigidbody>().AddForce(Bottom.transform.up * Time.deltaTime, ForceMode.Acceleration);
                    Destroy(ILE);
                    
                
                }
            }
            objectToSlice.Clear();
        }
    }

    void OnTriggerEnter(Collider col)
    {
        objectToSlice.Add(col.gameObject);
    }

    

    /**
    * Example on how to slice a GameObject in world coordinates.
    */
    public SlicedHull SliceObject(Vector3 planeWorldPosition,GameObject ile, Material mat = null) {
        Debug.Log("Blop");
	    return ile.Slice(transform.position, transform.up, mat);
        
}

}
