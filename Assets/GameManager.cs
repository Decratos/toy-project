﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    GameObject[] Spawner;
    public GameObject[] NMI = new GameObject[11];
    public bool attackMode;
    public GameObject Proj;
    public GameObject IA;
    public enum GameMode
    {
        Supa,
        Infection
    };
    // Start is called before the first frame update
    void Start()
    {
        NMI = GameObject.FindGameObjectsWithTag("AI");
        Spawner = GameObject.FindGameObjectsWithTag("Spawner");
        InvokeRepeating("ManageSpawner", 0, 3);
    }

    // Update is called once per frame
    void Update()
    {
        if(GameObject.FindGameObjectsWithTag("Proj").Length < 4)
        {
             float randX = Random.Range(-15f, 10);
            float randY = Random.Range(18.5f, -8f);
             Instantiate(Proj, new Vector3(randX, 5,randY), Quaternion.identity);
        }
    }

    private void ManageSpawner()
    {
        NMI = GameObject.FindGameObjectsWithTag("AI");
        if(attackMode)
        {
            foreach(GameObject S in Spawner)
            {
                if(NMI.Length < 10)
                {
                    var AI = Instantiate(IA, S.transform.position, Quaternion.identity);
                    AI.GetComponent<AIBehavior>().isInfected = true;
                }
                else
                    break;
                
            }
        }
    }
}
