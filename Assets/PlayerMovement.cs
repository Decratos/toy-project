﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Vector2 rotation = new Vector2(0,0);
    public GameObject objectPicker;
    public bool isPicking = false;
    public GameObject PickedObject;
    public GameManager GM;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        
        if(Input.GetKeyDown(KeyCode.E))
        {
            if(isPicking == false)
            {
                RaycastHit hit;
                Ray ray = Camera.main.ViewportPointToRay(new Vector3 (0.5f, 0.5f));
                    if(Physics.Raycast(ray, out hit, 10f))
                    {
                    if(hit.transform.tag == "proj" || hit.transform.tag == "wall")
                    {
                        hit.transform.position = objectPicker.transform.position;
                        hit.transform.SetParent(objectPicker.transform);
                        hit.transform.GetComponent<Rigidbody>().useGravity = false;
                        hit.transform.GetComponent<Rigidbody>().isKinematic = true;
                        hit.transform.localScale = hit.transform.localScale / 3;
                        PickedObject = hit.transform.gameObject;
                        isPicking = true;
                        GM.attackMode = true;
                    }
                    
                    }
                
                
            }
            else{
                    PickedObject.transform.SetParent(null);
                    PickedObject.transform.GetComponent<Rigidbody>().useGravity = true;
                    PickedObject.transform.GetComponent<Rigidbody>().isKinematic = false;
                    PickedObject = null;
                    isPicking = false;
                }
            
            
        }
        
    }

    void FixedUpdate()
    {
        if(Input.GetMouseButton(0))
        {
            if(isPicking && PickedObject != null)
            {
                PickedObject.transform.SetParent(null);
                //PickedObject.transform.GetComponent<Rigidbody>().useGravity = true;
                PickedObject.transform.GetComponent<Rigidbody>().useGravity = true;
                PickedObject.transform.GetComponent<Rigidbody>().isKinematic = false;
                PickedObject.GetComponent<Rigidbody>().AddForce(transform.forward * 2000);
                PickedObject.transform.localScale = PickedObject.transform.localScale * 3;
                PickedObject = null;
                isPicking = false;
            }
        }
    }

    void LateUpdate()
    {
        Move();
        MouseLook();
    }

    private void MouseLook()
    {
        if(Cursor.visible == false)
        {
            rotation.y += Input.GetAxis ("Mouse X");
            rotation.x += -Input.GetAxis ("Mouse Y");
            transform.eulerAngles = (Vector2)rotation * 3;
        }
        
    }

    private void Move()
    {
        if(Input.GetKey(KeyCode.Z))
        {
            transform.Translate(Vector3.forward * Time.deltaTime * 3);
        }
        else if(Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.back * Time.deltaTime * 3);
        }
        else if(Input.GetKey(KeyCode.Q))
        {
            transform.Translate(Vector3.left * Time.deltaTime * 3);
        }
        else if(Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * Time.deltaTime * 3);
        }
    }
}
