﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;

public class DestroyObject : MonoBehaviour
{
    public GameObject Debris;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            GameObject.Find("GameManager").GetComponent<GameManager>().attackMode = true;
            if(Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if(hit.transform.tag == "AI")
                {
                    Rigidbody[] R = hit.transform.gameObject.GetComponentsInChildren<Rigidbody>();
                    foreach(Rigidbody Rig in R)
                    {
                        var A = Rig.GetComponentInParent<NavMeshAgent>();
                        A.enabled = false;
                        Rig.isKinematic = false;
                    }
                    //Destroy(hit.transform.gameObject);
                }   
                
            }
        }
        if(Input.GetKeyDown(KeyCode.F5))
        {
            SceneManager.LoadScene("Infection");
        }
    }
}
